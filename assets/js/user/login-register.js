$(document).ready(function(){

    var translator = $('body').translate({lang: "es", t: dict});

    $("#login-form").on("submit", function(e){
        e.preventDefault();

        var form = $(this).serializeArray();
        //console.log(data);
        var data = convertToObject(form);
        $(this).request('Login::onAjax',
            {
                success: function(data){
                //alert("soy success");
                var translation ="";
                if(typeof data.X_OCTOBER_FLASH_MESSAGES !== 'undefined' && data.X_OCTOBER_FLASH_MESSAGES["error"] !== null){
                    var errorMessage = data.X_OCTOBER_FLASH_MESSAGES["error"];
                    if (typeof errorMessage!=="undefined" && errorMessage.endsWith("is temporarily blocked")) {
                         translation = "is temporarily blocked";
                    }else{
                         translation = translator.get(errorMessage);
                    }
                    //console.log(errorMessage);
                    
                    data.X_OCTOBER_FLASH_MESSAGES["error"] = translation;
                }
                if(typeof data.X_OCTOBER_FLASH_MESSAGES == "undefined" && data.X_OCTOBER_REDIRECT){
                    var location = window.location.pathname.split("/");
                    if(location.length == 5){
                        var baseRedirect = data.X_OCTOBER_REDIRECT;
                        data.X_OCTOBER_REDIRECT= baseRedirect+"/"+location[3]+"/"+location[4];
                    }
                     
                }else{
                    translation = translator.get(data.X_OCTOBER_FLASH_MESSAGES["success"]);
                    
                    //var lastIndex = (location.length);
            
                   // console.log("2",location); //AQUI QUEDE
                   // data.X_OCTOBER_FLASH_MESSAGES = translation;
                }
               // console.log("success", data);
                this.success(data); 
            },
            
            error: function(data){  
              console.log("error", data);
            }
        });
        //console.log(data);
    });

    $("#register-form").on("submit", function(e){
        e.preventDefault();
        var form = $(this).serializeArray();
        //console.log(data);
        var data = convertToObject(form);

        $(this).request('Registration::onAjax', {
            success: function(data){
                var translation ="";
                console.log(data);
                if(typeof data.X_OCTOBER_FLASH_MESSAGES !== 'undefined' && data.X_OCTOBER_FLASH_MESSAGES["error"] !== null){
                    var errorMessage = data.X_OCTOBER_FLASH_MESSAGES["error"];
                    if (errorMessage.endsWith("is busy")) {
                         translation = translator.get("busy");
                    }else{

                         translation = translator.get(errorMessage);
                    }
                    
                    data.X_OCTOBER_FLASH_MESSAGES["error"] = translation;
                }else{
                    
                    translation = translator.get(data.X_OCTOBER_FLASH_MESSAGES["success"]);
                    console.log(translation);
                    data.X_OCTOBER_FLASH_MESSAGES = translation;
                    
                }

                this.success(data);
            }
        });
    });

    $(".notification-wrapper").on("click", "#logout", function(){
        $.request('Logout::onAjax',
            {success: function(data){
                location.reload();
            },
            
            error: function(data){  
                location.reload();
            }
        
        });
    });
});