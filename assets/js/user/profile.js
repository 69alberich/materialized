$(document).ready(function(){
    //alert("sirvo");


    $(".address-type").on("change", function(){

      if($(this).val()=="shipping"){
         $("#country").val("VENEZUELA");
         $("#state").val("NUEVA ESPARTA");
         $("#country, #state").attr("readonly", true);
         $("#cities-select").prop("disabled", false);
         
      }else{
         
         $("#cities-select").prop("disabled", true);
         
         $("#country, #state").attr("readonly", false);
         $("#country, #state").val("");
      }
      //alert("sirvo");
      $("#city-text-content").toggleClass("hide");
      $("#cities-options-content").toggle();
    });
    
    $(".content-section:not(:first-child)").addClass("hide");

    $("#contact").on("submit", "#profile-form", function(e){
        e.preventDefault();
        $(this).request('UserPage::onAjax',{
        'update': {
            'profile/profile-form': '#contact',
            }
        });
    });
 //$('form').request('UserPage::onAjax');
    $("#payments-data-table").DataTable({
        "info":     false,
        "language": {
            "paginate": {
              "previous": "Anterior",
              "next" : "Siguiente",
            }
          },
          order: [ 1, "desc" ],
          scrollX: true,
          scrollCollapse: true,
          scroller:       true,
          "initComplete": function( settings, json ) {
           
            $("#orders-data-table").DataTable({
               "info":     false,
               "language": {
                   "paginate": {
                     "previous": "Anterior",
                     "next" : "Siguiente",
                   }
                 },
                 scrollX: true,
                 scrollCollapse: true,
                 scroller:       true,
             });
          }
      });
    
    

    $(".app-email").on("click", "#add-new-address", function(){
        $("#modal-address-form").modal("open");
    });

    $(".app-email").on("click", ".list-action", function(){

        var arrayChecked = [];
        $('.check-list:checked').each(
            function() {
                arrayChecked.push($(this).val());
               // alert("El checkbox con valor " + $(this).val() + " está seleccionado");
            }
        );
        var data = {
            'id' : arrayChecked
        }
        $.request('UserAddress::onRemove', {
            'data': data,
            'update': {
                'profile/addresses': '#main #addresses',
            }
        }).then(function(){
            if ($(".app-email .collection").length > 0) {
               var ps_email_collection = new PerfectScrollbar(".app-email .collection", {
                  theme: "dark"
               });
            }
        });
        //console.log(arrayChecked);
    });
    
    $("#address-form").on("submit", function(e){
        e.preventDefault();
        var form = $(this).serializeArray();
        //console.log(form);
        var data = convertToObject(form);
        //console.log(data);

        var errorMessage ="";
        if(data["type"]==="shipping"){
         if(data["address1"] === "" || data["address1"].length < 5){
            errorMessage+="*Tu dirección debe tener al menos 5 caracteres <br>";
           }
   
           if($("#cities-select").val() < 1){
            errorMessage+="*Tu ciudad debe tener al menos 3 caracteres <br>";
           }
        }else{
         if(data["country"]==="" || data["address1"].length < 4){
            errorMessage+="*Tu país debe tener al menos 4 caracteres <br>";
         }
         if(data["state"]==="" || data["state"].length < 4){
            errorMessage+="*Tu estado debe tener al menos 4 caracteres <br>";
         }
         if(data["city"]==="" || data["city"].length < 3){
            errorMessage+="*Tu ciudad debe tener al menos 3 caracteres <br>";
         }
        }
        

        if(errorMessage !==""){
         M.toast({html: errorMessage, classes: 'card-alert card gradient-45deg-red-pink'});
        }else{
           if(data["type"] === "shipping"){
            data["city"] = $("#cities-select option:selected").text();
           }

           $(this).request('UserAddress::onAdd', {
            'data': data,
            'update': {
                'profile/addresses': '#main #addresses',
            }
            }).then(function(){
                  $("#modal-address-form").modal("close");
                  $('#address-form')[0].reset();
                  if ($(".app-email .collection").length > 0) {
                     var ps_email_collection = new PerfectScrollbar(".app-email .collection", {
                        theme: "dark"
                     });
                  }
            });
        }
        
        /*if(!isNaN(data["city_id"])){
         data["city"] = $("#cities-select option:selected").text();
        }
        // data["country"] = $("#countries-select option:selected").text();
        
        if(data["state"] === null || !isNaN(data["state"])){
         data["state"] = $("#states-select option:selected").text();
        }
        
        if ($("#cities-select").val()) {
         data["city_id"] = $("#cities-select").val();
        }*/
        //console.log(data);


   
        
        //alert("sirvo");
        //console.log(data);
    });

    $("#address-form").on("change", ".location-options", function(){
      //alert("cambie!"+$(this).prop("name"));

      if ($(this).prop("name") === "country") {
         $.request('LocationManager::onChangeCountry', {
            data: {
               country : $(this).val()
             },
             'update': {
               'profile/states-options': '#states-options-content',
               'profile/cities-options': '#cities-options-content',
             }
         }).then( function() {
            var select = $("#address-form").find("#states-select");
            $(select).formSelect();
         });
      }else if($(this).prop("name") === "state") {
         $.request('LocationManager::onChangeState', {
            data: {
               state : $(this).val()
             },
             'update': {
               'profile/cities-options': '#cities-options-content',
             }
         }).then( function() {
            var select = $("#address-form").find("#cities-select");
            $(select).formSelect();
         });
      }
      
    });
    //DEFAULT
    "use strict";

   // For Modal
   $(".modal").modal();

   // Close other sidenav on click of any sidenav
   if ($(window).width() > 900) {
      $("#email-sidenav").removeClass("sidenav");
   }

   // TinyMCE Editor
   /*tinymce.init({
      selector: "#editor"
   });*/

   // Toggle class of sidenav
   $("#email-sidenav").sidenav({
      onOpenStart: function() {
         $("#sidebar-list").addClass("sidebar-show");
      },
      onCloseEnd: function() {
         $("#sidebar-list").removeClass("sidebar-show");
      }
   });

   //  Notifications & messages scrollable
   if ($("#sidebar-list").length > 0) {
      var ps_sidebar_list = new PerfectScrollbar("#sidebar-list", {
         theme: "dark"
      });
   }
   if ($(".app-email .collection").length > 0) {
      var ps_email_collection = new PerfectScrollbar(".app-email .collection", {
         theme: "dark"
      });
   }

   $("#email-sidenav a ").on("click", function() {
      var val = $(this).data("target");
      
      $("li").removeClass("active");
      $(this).addClass("active");

      $(".content-section").addClass("hide");
      $("#"+val).removeClass("hide");
   });

   /*$("#email-sidenav li").on("click", function(){
      alert("sirvo");
   });*/
   // Remove Row
   $('.app-email i[type="button"]').click(function(e) {
      $(this)
         .closest("tr")
         .remove();
   });

   // Favorite star click
   $(".app-email .favorite i").on("click", function(e) {
      e.preventDefault();
      $(this).toggleClass("amber-text");
   });

   // Important label click
   $(".app-email .email-label i").on("click", function(e) {
      e.preventDefault();
      $(this).toggleClass("amber-text");
      if ($(this).text() == "label_outline") $(this).text("label");
      else $(this).text("label_outline");
   });

   // To delete all mails
   $(".app-email .delete-mails").on("click", function() {
      $(".collection-item")
         .find("input:checked")
         .closest(".collection-item")
         .remove();
   });

   // To delete Single mail
   $(".app-email .delete-task").on("click", function() {
      $(this)
         .closest(".collection-item")
         .remove();
   });

   // Sidenav
   $(".sidenav-trigger").on("click", function() {
      if ($(window).width() < 960) {
         $(".sidenav").sidenav("close");
         $(".app-sidebar").sidenav("close");
      }
   });
});


// Checkbox
function toggle(source) {
    checkboxes = document.getElementsByName("foo");
    for (var i = 0, n = checkboxes.length; i < n; i++) {
       checkboxes[i].checked = source.checked;
    }
 }
 
 $(window).on("resize", function() {
    resizetable();
 
    if ($(window).width() > 899) {
       $("#email-sidenav").removeClass("sidenav");
    }
 
    if ($(window).width() < 900) {
       $("#email-sidenav").addClass("sidenav");
    }
 });
 function resizetable() {
    $(".app-email .collection").css({
       maxHeight: $(window).height() - 380 + "px"
    });
 }
 resizetable();