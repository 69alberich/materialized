$(document).ready(function(){

    $("#restore-form").on("submit", function(e){
        e.preventDefault();
        $(this).request('RestorePassword::onAjax');
    });


    $("#reset-form").on("submit", function(e){
        e.preventDefault();
        $(this).request('ResetPassword::onAjax');
    });

});