$("document").ready(function(){
    $('.js-range-slider').ionRangeSlider();
    $('.carousel').carousel({
        numVisible: 3,
        dist: -80,
        padding: 5
    });
    $('.parent-container').magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        // other options
        gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				//return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
			}
		}
      });
    $('.materialboxed').materialbox({
        onOpenStart: function(){
            $(".owl-carousel, .card-owl-carousel").css("z-index", -1);
        },
        onCloseEnd: function(){
            $(".owl-carousel, .card-owl-carousel").css("z-index", 0);
        }
     });

    $("#product-main-content").on("change", " .icons", function(){
        var maxQuantity = $(this).find("option:selected").data("max-quantity");
        var quantity = 0;
        if(maxQuantity >= 20){
            quantity = 20;
        }else{
            quantity = maxQuantity;
        }
        var my_range = $("#product-main-content .js-range-slider").data("ionRangeSlider");

        my_range.update({
            from: 0,
            max: quantity
        });

        $("#product-main-content .offer-price").hide();
        $("#product-main-content #offer-price-"+$(this).val()).removeClass("hide").show();
    });

    $('.owl-carousel.brands-carousel').owlCarousel({
        margin:25,
        stagePadding: 0,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:false
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:4,
                nav:false,
                loop:false
            }
        }
    });

    $('.owl-carousel.newest-products').owlCarousel({
        margin:25,
        stagePadding: 50,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:false,
                stagePadding: 5,
            },
            600:{
                items:2,
                nav:false
            },
            1000:{
                items:2,
                nav:false,
                loop:false
            }
        }
    });
});