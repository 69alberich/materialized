$("document").ready(function(){

    $('.dropify').dropify();
    
    $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true
      });
    
    $('.collapsible').collapsible({
    accordion:true
    });

    $(".notification-wrapper").on("click",".currency-option", function(){
        //alert("sirvo");
        var value = $(this).data("code");
        
        $.request('CurrencyList::onSwitch', {

            data: {currency: value}
          }).then(function(){
              location.reload();
          });
    });
    
    var stepper = document.querySelector('.stepper');

     if(typeof stepper !== "undefined" && stepper !== null ) {
        var stepperInstace = new MStepper(stepper, {
            // options
            firstActive: 0, // this is the default
            validationFunction: validationFunction,
        });    
        function validationFunction(stepperForm, activeStepContent) {
            // You can use the 'stepperForm' to valide the whole form around the stepper:
            //someValidationPlugin(stepperForm);
            // Or you can do something with just the activeStepContent
            return validateStepper(stepperForm, activeStepContent);
            // Return true or false to proceed or show an error
            //return true;
         }
        $("#shipping-list-content").on("change", ".shipping-type", function(){
            //console.log($(this).val());
            $("#shipping-address-fields").toggleClass("hide");
            if($(this).val() > 1 ){

                //console.log("pongo el formulario y quito el letrero");
                $("#shipping-address-fields input, #shipping-address-fields textarea").prop("disabled", false);
                
                $("#shipping-address-link").removeClass("hide");  
                $("#subsidiary-content").addClass("hide");
            }else{
                
                $("#shipping-address-fields").addClass("hide");
                
                $("#shipping-address-fields input, #shipping-address-fields textarea").prop("disabled", true);
                $("#shipping-address-link").addClass("hide");
                $("#subsidiary-content").removeClass("hide");
                //console.log("quito el formulario y pongo el letrero");      
                //stepperInstace.deactivateStep(stepAddress);
            }
        });
    }
    function validateStepper(stepperForm, activeStepContent){
        var form = $(stepperForm).serializeArray();
        var data = convertToObject(form);
        var message="";
        if ($(activeStepContent).attr("id")=="client-step") {
            //alert("en el primero");
            if(data["firstname"]=="" || data["firstname"].lenght <4){
                message+="*Tu nombre debe tener al menos 4 caracteres<br>";
            }
            if(data["lastname"]=="" || data["lastname"].lenght <4){
                message+="*Tu apellido debe tener al menos 4 caracteres<br>";
            }
            if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data["email"])){
                message+="*Tu email no es válido<br>";
            }
            if(data["phone"]=="" || data["phone"].lenght <9){
                message+="*Tu apellido debe tener al menos 9 caracteres<br>";
            }
           
        }else if($(activeStepContent).attr("id")=="address-time-step"){
            if(data["shipping_date"]==""){
                message+="*Elige una fecha de entrega/retiro<br>";
            }

            if(data["shipping_time"] === "" || typeof data["shipping_time"] === 'undefined'){
                message+="*Elige una hora de entrega/retiro<br>";
            }

            if(data["shipping_type_id"] !== "1"){
                if(data["shipping_city"] === "" || typeof data["shipping_city"] === 'undefined'){
                    message+="*Elige una ciudad<br>";
                }
    
                if(data["shipping_address1"]=="" || data["shipping_address1"].lenght <15){
                    message+="*Tu dirección debe tener al menos 15 caracteres<br>";
                }
            }
            
            //console.log(data);
        }
        if (message!="") {
            M.toast({html: message, classes: 'card-alert card gradient-45deg-red-pink'});
            return false;
        }else{
            return true
        }
        //console.log(formData);
    }
    /*$(".owl-carousel").owlCarousel({
        items: 1,
        stagePadding: 25,
        responsive:{
            360:{
                items: 1,
            },
            480 : {
                items : 2,
            },
            // breakpoint from 768 up
            768 : {
                items : 1,
            }
        }
      }
      
    );*/
    
    $(".owl-carousel-3").owlCarousel({

       nav: false,
       dots: false,
       items: 3,

      });

    $("#main").on("click", ".product-trigger", function(){
        var productId = $(this).data("product-id");
        var data ={
            'productId': productId,
        }
        $.request('ProductHandler::onCheckProduct', {
            'data': data,
            'update': {
                'modals/product-modal': '#modal-product'
            }
        }).then(function(){
            $(".modal-product").modal("open");
            $('select').formSelect();
            $('.js-range-slider').ionRangeSlider();
        });
        
    });
    $("#modal-product").on("change", ".modal-form .icons", function(){
        var maxQuantity = $(this).find("option:selected").data("max-quantity");
        var quantity = 0;
        if(maxQuantity >= 20){
            quantity = 20;
        }else{
            quantity = maxQuantity;
        }
        var my_range = $("#modal-product .js-range-slider").data("ionRangeSlider");

        my_range.update({
            from: 0,
            max: quantity
        });
        //alert("tengo:"+$(this).val());
        $("#modal-product .modal-price").hide();
        $("#offer-price-"+$(this).val()).removeClass("hide").show();
    
    });
    $("#modal-product").on("submit", "#product-modal-form", function(e){
        e.preventDefault();
        var form = $(this).serializeArray();
        var formData = convertToObject(form);
        var data = {
            'cart': [
                {'offer_id': formData.offer, 'quantity': formData.quantity}
            ],
            'shipping_type_id': 1
        };
        
        //Send ajax request and update cart items
        $.request('Cart::onAdd', {
            'data': data,
            'update': {
                'main-nav': '.notification-wrapper',
                'aside-options': '.cart-item-wrapper',
                'cart/cart-product-list': '#cart-list-container',
                'cart/cart-total-price': '#cart-total-container',
                'cart/shipping-list' : '#shipping-list-content',
            }
        }).then(function(){
            $(".modal").modal("close");
            $(".profile-button").dropdown({
                inDuration: 300,
                outDuration: 225,
                constrainWidth: false,
                hover: false,
                gutter: 0,
                coverTrigger: false,
                alignment: "right"
                // stopPropagation: false
             });
            M.toast({html: CART_TOAST_SUCCESS, classes: 'card-alert card gradient-45deg-light-blue-cyan'});
        });
    });

    $("#single-product-form").on("submit", function(e){
        e.preventDefault();
        var form = $(this).serializeArray();
        var formData = convertToObject(form);

        var data = {
            'cart': [
                {'offer_id': formData.offer, 'quantity': formData.quantity}
            ],
            'shipping_type_id': 1
        };
        
        //Send ajax request and update cart items
        $.request('Cart::onAdd', {
            'data': data,
            'update': {
                'main-nav': '.notification-wrapper',
                'aside-options': '.cart-item-wrapper'
            }
        }).then(function(){
            M.toast({html: CART_TOAST_SUCCESS, classes: 'card-alert card gradient-45deg-purple-deep-orange'});
        });
    });

    $("#content-cart-list, #cart-list-container").on("click", ".remove-from-cart", function(){
        var offerId = $(this).data("offer-id");
        var data = {
            'cart': [offerId],
            'shipping_type_id': 4
        };
        
        //Send ajax request and update cart items
        $.request('Cart::onRemove', {
            'data': data,
            'update': {
                'aside-options': '.cart-item-wrapper',
                'main-nav': '.notification-wrapper',
                'cart/cart-product-list': '#cart-list-container',
                'cart/shipping-list' : '#shipping-list-content',
            }
        }).then(function(){
            $(".profile-button").dropdown({
                inDuration: 300,
                outDuration: 225,
                constrainWidth: false,
                hover: false,
                gutter: 0,
                coverTrigger: false,
                alignment: "right"
                // stopPropagation: false
             });
        });
    });

    $("#content-cart-list, #cart-list-container").on("click", ".clear-cart", function(){

        $.request('Cart::onClear', {
            'update': {
                'aside-options': '.cart-item-wrapper',
                'main-nav': '.notification-wrapper',
                'cart/cart-product-list': '#cart-list-container',
                'cart/cart-total-price': '#cart-total-container',

            }
        }).then(function(){
            $(".profile-button").dropdown({
                inDuration: 300,
                outDuration: 225,
                constrainWidth: false,
                hover: false,
                gutter: 0,
                coverTrigger: false,
                alignment: "right"
                // stopPropagation: false
             });
        });

    });
   

    $("#order-form").on("submit", function(e){
        e.preventDefault();
        var  form = $(this).serializeArray();

        var formData = convertToObject(form);
        //console.log(formData);
        
        var shippingCountry = "";
        var billingCountry = "";

        if($("#shipping-countries-select").val() > 0){
            shippingCountry = $("#shipping-countries-select option:selected").text();
        }
        //if($("#billing-countries-select").val() > 0){
            billingCountry = $("#billing-countries-select option:selected").text();
       // }
        var data = {
            'order': {
                'shipping_type_id': formData.shipping_type_id,
                'property':{
                    'shipping_country': shippingCountry,
                    'shipping_state': formData.shipping_state,
                    'shipping_city': formData.shipping_city,
                    'shipping_address1': formData.shipping_address1,
                    'billing_country': billingCountry,
                    'billing_state': formData.billing_state,
                    'billing_city': formData.billing_city,
                    'billing_address1': formData.billing_address1,
                    'shipping_date': formData.shipping_date,
                    'shipping_time': formData.shipping_time
                }  
            },
            'user': {
                'email': formData.email,
                'name': formData.firstname,
                'last_name':formData.lastname,
                'phone':formData.phone
            },
        }
        if(typeof formData.subsidiary !=="undefined"){
            data["order"]["property"]["subsidiary"] = formData.subsidiary;
        }
        //console.log(data);
        
        $.request('MakeOrder::onCreate', {
            'data': data,
            'success': function(data){
                //redirect a data.key
                //console.log(data);
                //alert(datakey);
                $(location).attr('href', 'http://farmaciaestes.com/orden/'+data.data.key);
                //window.location.href = "http://localhost/sandbox/orden/"+data[key;
            }
        });
        
    });

    $("#calendar-content").on("change", "#shippingTime", function(){
       //alert("sirvo"); 
    });


    $("#order-form").on("change", ".location-options", function(){
        //alert("cambie!"+$(this).prop("name"));
  
        if ($(this).prop("name") === "shipping_country") {
           $.request('LocationManager::onChangeCountry', {
              data: {
                 country : $(this).val()
               },
               'update': {
                 'cart/states-options': '#states-options-content',
                 'cart/cities-options': '#cities-options-content',
               }
           }).then( function() {
              var select = $("#order-form").find("#states-options-content select");
              select.prop("name", "shipping_state");
              
              var textfield = $("#order-form").find("#states-options-content input");
              textfield.prop("name", "shipping_state");
              
              var textfield = $("#order-form").find("#cities-options-content input");
              textfield.prop("name", "shipping_city");


              $(select).formSelect();
           });
        }else if($(this).prop("name") === "shipping_state") {
           $.request('LocationManager::onChangeState', {
              data: {
                 state : $(this).val()
               },
               'update': {
                 'cart/cities-options': '#cities-options-content',
               }
           }).then( function() {
              var select = $("#order-form").find("#cities-options-content select");
              select.prop("name", "shipping_city");

              var textfield = $("#order-form").find("#cities-options-content input");
              textfield.prop("name", "shipping_city");

              $(select).formSelect();
           });
        }else if($(this).prop("name") === "billing_country"){
            $.request('LocationManager::onChangeCountry', {
                data: {
                   country : $(this).val()
                 },
                 'update': {
                   'cart/states-options': '#billing-states-options-content',
                   'cart/cities-options': '#billing-cities-options-content',
                 }
             }).then( function() {
                var select = $("#order-form").find("#billing-states-options-content select");
                $(select).formSelect();

                var textfield = $("#order-form").find("#billing-states-options-content input");
                textfield.prop("name", "billing_state");
                
                var textfield = $("#order-form").find("#billing-cities-options-content input");
                textfield.prop("name", "billing_city");
             });
        }else if($(this).prop("name") === "billing_state"){
            $.request('LocationManager::onChangeState', {
                data: {
                   state : $(this).val()
                 },
                 'update': {
                   'cart/cities-options': '#billing-cities-options-content',
                 }
             }).then( function() {
                var select = $("#order-form").find("#billing-cities-options-content select");
                select.prop("name", "billing_city");
  
                var textfield = $("#order-form").find("#billing-cities-options-content input");
                textfield.prop("name", "billing_city");
  
                $(select).formSelect();
             });
            
        }
        
      });

      $("#address-form-modal").on("submit", function(e){
        e.preventDefault();
        //alert("sirvo");
        
        $(this).request('LocationManager::onSelectSavedAddress',
        {
          'update': {
              'cart/shipping-address-aux-form': '#shipping-address-fields'
          }
        }).then(function(){
            var select = $("#shipping-address-fields").find(".location-options");
            $(select).formSelect();
            M.updateTextFields();
            //stepperInstace.resetStepper();

            $("#modal-address-form-window").modal("close");
        });
      });

      $("#billing-address-form-modal").on("submit", function(e){
        e.preventDefault();
        //alert("sirvo");
        
        $(this).request('LocationManager::onSelectSavedAddress',
        {
          'update': {
              'cart/billing-address-aux-form': '#billing-address-fields'
          }
        }).then(function(){
            var select = $("#billing-address-fields").find(".location-options");
            $(select).formSelect();
            M.updateTextFields();
            //stepperInstace.resetStepper();

            $("#modal-billing-address-form-window").modal("close");
        });
      });

      

      $("#shipping-address-link").on("click", function(){
        $("#modal-address-form-window").modal("open");
      });
      
      $("#billing-address-link").on("click", function(){
        $("#modal-billing-address-form-window").modal("open");
      });

    $('#payment-form').on('submit', function(e){
       
        var request = $(this).payStripe();
        
        request.then(function(response){
        var respuesta = JSON.parse(response.result);
        //console.log("respuesta", respuesta);
        if(respuesta.status === "succeeded"){
            //abre aqui
            var alerta_stripe = swal({
            title: 'Hemos procesado su pago correctamente, ',
            text: 'Espere mientras registramos su compra',
            closeOnEsc:false,
            allowEnterKey: false,
            allowOutsideClick: false,
            button:true,
            }).then( response => {
            var data = {
                reference: respuesta["reference"],
                method_id: 1,
                currency: 1,
                status_id: 1,
                order_status_id: 5,
                
            }
            /*if(typeof $("#subsidiary") !== "undefined" && $("#subsidiary").val()!= null ){
                data["subsidiary"] = $("#subsidiary").val();
            }*/
                $.request('PaymentsHandler::onAddPayment', {
                    data: data,
                    success: function(response) {
                        swal.close();
                        swal({
                        title: '¡Proceso Completado!',
                        content: 'Gracias por Reservar con nosotros',
                        
                        }).then(function(){
                            location.reload();
                        }

                        );
                    },
                    error: function(response){
                    swal.close();
                    swal({
                    
                        title: 'Ha ocurrido un error',
                        content: 'Intenta nuevamente por favor',
                        onClose: function(){
                        location.reload();
                        }
                    });
                    }
                });
  
            },
            function(){
            //
            });
            //termina aqui
            
        }else{
            //console.log(response);
            swal({
            title: 'Ha ocurrido un error',
            content: "Intenta nuevamente por favor",
            onClose: function(){
                location.reload();
            }
            });
        }
        },
        function(response){
        swal({
            title: 'Disculpe, Ha fallado la pasarela de pagos',
        }).then(function(){
            location.reload()
        });
        });
        e.preventDefault();
    });
    
    $(".file-payment-form").on("submit", function(e){
        e.preventDefault();
        $(this).request('PaymentsHandler::onAddPayment', {
    
            'update': {
                'modals/product-modal': '#modal-product'
            },
            'success': function(response){
                swal({
                    title: "¡Bien hecho!",
                    text: "Hemos recibido tu comprobante",
                    icon: "success",
                    button: "aceptar",
                  }).then((value)=>{
                      location.reload();
                  });
            },
            'error': function(response){
                //console.log(response);
                swal({
                    title: "¡Oops!",
                    text: "Ha ocurrido un error, intenta nuevamente",
                    icon: "error",
                    button: "aceptar",
                  }).then((value)=>{
                      location.reload();
                  });
            }
        });
       
    });
    $("#pay-in-site-form").on("submit", function(e){
        e.preventDefault();
        $(this).request('PaymentsHandler::onPayInSite', {

            'success': function(response){
                swal({
                    title: "¡Bien hecho!",
                    text: "Te esperamos en la sede que elegiste para concretar tu compra",
                    icon: "success",
                    button: "aceptar",
                  }).then((value)=>{
                      location.reload();
                  });
            },
            'error': function(response){
                //console.log(response);
                swal({
                    title: "¡Oops!",
                    text: "Ha ocurrido un error, intenta nuevamente",
                    icon: "error",
                    button: "aceptar",
                  }).then((value)=>{
                      location.reload();
                  });
            }
        });
    });
});

