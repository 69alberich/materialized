$("document").ready(function(){


   searchSm = $(".search-sm"),
   searchBoxSm = $(".search-input-sm .search-box-sm"),
   searchListSm = $(".search-list-sm");

    $(".search-button").click(function (e) {
        if (searchSm.is(":hidden")) {
           searchSm.show();
           searchBoxSm.focus();
        } else {
           searchSm.hide();
           searchBoxSm.val("");
        }
     });
     $(".search-sm-close").click(function (e) {
        searchSm.hide();
        searchBoxSm.val("");
     });
    
     autoCompleteFlag = true;

     $(".autocomplete").autocomplete({
        minLength: 3,
        onAutocomplete: function(){
            var element = $(this)[0].el;
            var form = $(element).parents("form").first()
            form.trigger("submit");
        }
      });
      $(".autocomplete").on("keypress", function(){
          var value = $(this).val();
          
          if(value.length > 2 && autoCompleteFlag == true ){
            autoCompleteFlag = false;
            $(this).request('SearchHandler::onAutoComplete', {
                success: (response =>{
                    $(this).autocomplete('updateData', response);
                })
            }).then(()=>{
                autoCompleteFlag = true;
                $(this).autocomplete('open');

                //alert("completado");
            });
          }else{
              //console.log("no mando", autoCompleteFlag);
          }
      });
      
    /*
      $(element).updateData({
        "Apple2": null,
        "Microsoft2": null,
        "Google2": 'https://placehold.it/250x250'
      });*/

   // console.log(element);

    $(".filter-control").on("change", function(){
         var form = $(this).parents("form").first();
         form.trigger("submit");
    });

    $(".filter-form").on("submit", function(e){
        e.preventDefault();
        
        var form = $(this).serializeArray();
        //console.log(data);
        var data = convertToObject(form);
        console.log(data);
        
        $.request('SearchHandler::onFilter', {
            'data': data,
            'update': {
                'products/products-results': '#result-container'
            }
        }).then(function(){
            //alert("completado");
        });
    });

    $(".search-form").on("submit", function(e){
        e.preventDefault();
        $(this).request('SearchHandler::onSearch');
    });

    $("#result-container").on("click", ".pagination-control", function(){
       var flag = null;
       var selectedForm = null;
       var pageValue = $(this).data("value");
       $(".filter-form").each(function(index, element){
           if(flag == null){
                flag = $(element).is(":hidden");
                selectedForm = element;
            }else if(flag == true){
                flag = true;
            }else{
                flag = false;
                selectedForm = element;
            }
            //console.log($(element).is(":hidden"));
       });
       //SI FLAG ES FALSE SE USO EL FILTRO DESK SINO EL MOBILE
       var forms = $(".filter-form");

       if (flag) {
        //console.log($(forms[1]).serializeArray());
        var form = $(forms[1]).serializeArray();
       }else{
        //console.log($(forms[0]).serializeArray());
        var form = $(forms[0]).serializeArray();
       }
       
       var data = convertToObject(form);
       data["page"] = pageValue;

       $.request('SearchHandler::onFilter', {
        'data': data,
        'update': {
            'products/products-results': '#result-container'
        }
        }).then(function(){
            //alert("completado");
        });
        //alert($(this).data("value"));
    });
});