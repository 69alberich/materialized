$("document").ready(function(){
    
    $('#payment-form-test').on('submit', function(e){
        var request = $(this).payStripe();
        e.preventDefault();
        request.then(function(response){
        var respuesta = JSON.parse(response.result);
        console.log("respuesta", respuesta);
            if(respuesta.status === "succeeded"){
                //abre aqui
                var alerta_stripe = swal({
                title: 'Hemos procesado su pago correctamente, ',
                text: 'Espere mientras registramos su compra',
                closeOnEsc:false,
                allowEnterKey: false,
                allowOutsideClick: false,
                button:true,
                });
                //termina aqui
            }else{
                //console.log(response);
                swal({
                title: 'Ha ocurrido un error',
                content: respuesta.message,
                onClose: function(){
                    location.reload();
                }
                });
            }
        },
        function(response){
            swal({
                title: 'Disculpe, Ha fallado la pasarela de pagos',
            }).then(function(){
                location.reload()
            });
        });
        
    });
})